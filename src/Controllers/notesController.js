const { Note } = require('../models/Note');

async function createNotes(req, res, next) {
  try {
    const { text } = req.body;
    if (!text) {
      res.status(400).json({ message: 'The field must not be empty' });
    }
    const note = new Note({
      userId: req.user.userId,
      text,

    });
    await note.save();
      res.status(200).json(
        {
          message: 'Success',
        },
      );
  } catch (error) {
    res.status(500).json({
      message: 'Server error',
    });
  }
}

async function getNotes(req, res, next) {
  const queryParameter = req.query;
  let limit = Number(queryParameter.limit);
  let offset = Number(queryParameter.offset);

  if (!limit) {
    limit = 0;
  }
  if (!offset) {
    offset = 0;
  }

  const notes = await Note.find({ userId: req.user.userId }, '-__v');
  
      res.status(200).json({
        offset,
        limit,
        count: notes.length,
        notes,
      });
}

const getNote = async (req, res, next) => {
try {
const note = await Note.findById(req.params.id);
    res.status(200).json({
      note,
    });
} catch (error) {
  res.status(500).json({ message: 'My server error' }) 
}
}

const updateMyNoteById = async (req, res, next) => {
  try {
    const content = req.body.text;
    if (content) {
      await Note.findByIdAndUpdate(
        { _id: req.params.id, userId: req.user.userId },
        { text: content },
      );
      res.status(200).json({ message: 'Success' });
    } else {
      res.status(400).json({ message: 'The field must not be empty' });
    }
  } catch (err) {
    res.status(500).json({ message: 'My server error' });
  }
};

const markMyNoteCompletedById = async (req, res, next) => {
  try {
    const note = await Note.findByIdAndUpdate({
      _id: req.params.id,
      userId: req.user.userId,
    }); 
    note.completed = !note.completed;
    await note.save();
      res.status(200).json({
        message: 'Success',
      });
  } catch (err) {
    res.status(500).json({
      message: 'Server error',
    });
  }
};

const deleteNote = async (req, res, next) => {
  try {
    await Note.findByIdAndDelete(req.params.id);
    res.status(200).json(
      {
        message: 'Success',
      },
    );
  } catch (err) {
    res.status(400).json({
      message: 'Server error',
    });
  }
};

module.exports = {
  createNotes,
  getNotes,
  getNote,
  updateMyNoteById,
  markMyNoteCompletedById,
  deleteNote,
};
