const { User } = require('../models/User');
const bcrypt = require('bcryptjs');

async function getUser(req, res, next) {
  try {
    const { _id, username, createdDate } = await User.findOne({ _id: req.user.userId });
    res.status(200).json({
      user: {
        _id,
    username,
    createdDate,
      },
    });
   
  } catch (error) {
    res.status(400).json({
      message: 'This user does not exist',
    });
  }
}

async function editPasswordUser(req, res, next) {
  try {
    
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const {userId , username} = req.user;
    const user = await User.findOne({username});
   
    if (oldPassword && newPassword && await bcrypt.compare(
      String(oldPassword),
      String(user.password)
    )) {
      let newPasswordHash = await bcrypt.hash(newPassword, 10);
      console.log('newPasswordHash='+newPasswordHash);
      let data = await User.findByIdAndUpdate(userId, {$set: {password: newPasswordHash} });

        res.status(200).json({
          "message": "Success"
        });
      
    } else {
      res.status(400).json({ message: 'Password not changed' });
    }
  } catch (error) {
    res.status(500).json({
      message: 'My Server error'
    });
}
}

async function deleteUser(req, res, next) {
  try {
    const { user } = req;
    await User.deleteOne(user);

    if (!user) {
      res.status(400).json({ message: 'This user does not exist' });
    }
    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    res.status(400).json({
      message: 'Server error',
    });
  }
}

module.exports = {
  getUser,
  editPasswordUser,
  deleteUser,
};
