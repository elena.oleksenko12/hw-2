const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/User');

const registerUser = async (req, res, next) => {
  try {
  const { username, password } = req.body;
  if (!username || !password) {
    res.status(400).json({ 'message': 'The field must not be empty' });
  }

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();
      res.status(200).json({
        'message': 'Success',
      });
} catch (error) {
  return res.status(500).json({ message: error.message })
}
}

const loginUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    if (user && await bcrypt.compare(
      String(req.body.password),
      String(user.password),
    )) {
      const payload = {
        username: user.username,
        userId: user._id,
        createdDate: user.createdDate,
      };
      const jwtToken = jwt.sign(payload, process.env.secret_jwt_key);
      res.status(200).json({
        'message': 'Success',
        jwt_token: jwtToken,
      });
      return;
    }
      res.status(400).json({ 'message': 'Not authorized' });
  } catch (err) {
    res.status(500).json({ 'message': 'Server error' });
  }
};

module.exports = {
  registerUser,
  loginUser,
};
