const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

const PORT = 8080;
require('dotenv').config();
// console.log(process.env);

mongoose.connect('mongodb+srv://OlenaOleksenko:5227337109531N@olenaoleksenkomongodbcl.jwck9ba.mongodb.net/users?retryWrites=true&w=majority');

const { authRouter } = require('./Routers/authRouter');
const { userRouter } = require('./Routers/userRouter');
const { notesRouter } = require('./Routers/notesRouter');
const { authMiddleware } = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, userRouter);
app.use('/api/notes', authMiddleware, notesRouter);

const start = async () => {
  try {
    app.listen(PORT, () => {
      console.log(`Server started on ${PORT}`);
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
