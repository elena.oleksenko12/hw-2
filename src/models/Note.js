const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const NoteSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

NoteSchema.plugin(mongoosePaginate);

const Note = mongoose.model('Note', NoteSchema);

module.exports = {
  Note,
};
