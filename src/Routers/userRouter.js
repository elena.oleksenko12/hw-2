const express = require('express');

const router = express.Router();
const { getUser, deleteUser, editPasswordUser } = require('../Controllers/userController');

router.get('/me', getUser);
router.delete('/me', deleteUser);
router.patch('/me', editPasswordUser);

module.exports = {
  userRouter: router,
};
