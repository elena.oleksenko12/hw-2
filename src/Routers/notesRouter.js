const express = require('express');

const router = express.Router();
const {
  createNotes,
  getNotes,
  getNote,
  updateMyNoteById,
  markMyNoteCompletedById,
  deleteNote,
} = require('../Controllers/notesController');

router.get('/', getNotes);
router.post('/', createNotes);
router.get('/:id', getNote);
router.put('/:id', updateMyNoteById);
router.patch('/:id', markMyNoteCompletedById);
router.delete('/:id', deleteNote);

module.exports = {
  notesRouter: router,
};
